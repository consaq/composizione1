/* ------------------------------------------------------------
name: "AnalisiPicchi"
Code generated with Faust 2.74.6 (https://faust.grame.fr)
Compilation options: -lang cpp -ct 1 -es 1 -mcd 16 -mdd 1024 -mdy 33 -single -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <math.h>

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif

#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

#if defined(_WIN32)
#define RESTRICT __restrict
#else
#define RESTRICT __restrict__
#endif

static float mydsp_faustpower2_f(float value) {
	return value * value;
}

class mydsp : public dsp {
	
 private:
	
	FAUSTFLOAT fVbargraph0;
	FAUSTFLOAT fVbargraph1;
	FAUSTFLOAT fVbargraph2;
	int fSampleRate;
	float fConst0;
	float fConst1;
	float fRec0[2];
	float fConst2;
	int IOTA0;
	float fVec0[32768];
	float fConst3;
	int iConst4;
	float fRec1[2];
	int iVec1[2];
	int iRec2[2];
	int iVec2[2];
	
 public:
	mydsp() {
	}
	
	void metadata(Meta* m) { 
		m->declare("basics.lib/name", "Faust Basic Element Library");
		m->declare("basics.lib/tabulateNd", "Copyright (C) 2023 Bart Brouns <bart@magnetophon.nl>");
		m->declare("basics.lib/version", "1.18.0");
		m->declare("compile_options", "-lang cpp -ct 1 -es 1 -mcd 16 -mdd 1024 -mdy 33 -single -ftz 0");
		m->declare("filename", "AnalisiPicchi.dsp");
		m->declare("filters.lib/lowpass0_highpass1", "MIT-style STK-4.3 license");
		m->declare("filters.lib/name", "Faust Filters Library");
		m->declare("filters.lib/pole:author", "Julius O. Smith III");
		m->declare("filters.lib/pole:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/pole:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/version", "1.3.0");
		m->declare("maths.lib/author", "GRAME");
		m->declare("maths.lib/copyright", "GRAME");
		m->declare("maths.lib/license", "LGPL with exception");
		m->declare("maths.lib/name", "Faust Math Library");
		m->declare("maths.lib/version", "2.8.0");
		m->declare("name", "AnalisiPicchi");
		m->declare("platform.lib/name", "Generic Platform Library");
		m->declare("platform.lib/version", "1.3.0");
	}

	virtual int getNumInputs() {
		return 2;
	}
	virtual int getNumOutputs() {
		return 4;
	}
	
	static void classInit(int sample_rate) {
	}
	
	virtual void instanceConstants(int sample_rate) {
		fSampleRate = sample_rate;
		fConst0 = std::min<float>(1.92e+05f, std::max<float>(1.0f, float(fSampleRate)));
		fConst1 = std::exp(-(62.831852f / fConst0));
		fConst2 = 1e+01f / fConst0;
		fConst3 = 0.1f * fConst0;
		iConst4 = int(fConst3);
	}
	
	virtual void instanceResetUserInterface() {
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; l0 < 2; l0 = l0 + 1) {
			fRec0[l0] = 0.0f;
		}
		IOTA0 = 0;
		for (int l1 = 0; l1 < 32768; l1 = l1 + 1) {
			fVec0[l1] = 0.0f;
		}
		for (int l2 = 0; l2 < 2; l2 = l2 + 1) {
			fRec1[l2] = 0.0f;
		}
		for (int l3 = 0; l3 < 2; l3 = l3 + 1) {
			iVec1[l3] = 0;
		}
		for (int l4 = 0; l4 < 2; l4 = l4 + 1) {
			iRec2[l4] = 0;
		}
		for (int l5 = 0; l5 < 2; l5 = l5 + 1) {
			iVec2[l5] = 0;
		}
	}
	
	virtual void init(int sample_rate) {
		classInit(sample_rate);
		instanceInit(sample_rate);
	}
	
	virtual void instanceInit(int sample_rate) {
		instanceConstants(sample_rate);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	
	virtual int getSampleRate() {
		return fSampleRate;
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openHorizontalBox("sig");
		ui_interface->declare(0, "0", "");
		ui_interface->openHorizontalBox("input");
		ui_interface->addVerticalBargraph("Level 0", &fVbargraph0, FAUSTFLOAT(-6e+01f), FAUSTFLOAT(0.0f));
		ui_interface->addVerticalBargraph("Level 1", &fVbargraph1, FAUSTFLOAT(-6e+01f), FAUSTFLOAT(0.0f));
		ui_interface->closeBox();
		ui_interface->declare(&fVbargraph2, "2", "");
		ui_interface->addVerticalBargraph("Input Mid", &fVbargraph2, FAUSTFLOAT(-6e+01f), FAUSTFLOAT(0.0f));
		ui_interface->closeBox();
	}
	
	virtual void compute(int count, FAUSTFLOAT** RESTRICT inputs, FAUSTFLOAT** RESTRICT outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		for (int i0 = 0; i0 < count; i0 = i0 + 1) {
			float fTemp0 = float(input0[i0]);
			fVbargraph0 = FAUSTFLOAT(2e+01f * std::log10(std::max<float>(1.1754944e-38f, std::fabs(fTemp0))));
			float fTemp1 = float(input1[i0]);
			fVbargraph1 = FAUSTFLOAT(2e+01f * std::log10(std::max<float>(1.1754944e-38f, std::fabs(fTemp1))));
			float fTemp2 = fTemp0 + fTemp1;
			fVbargraph2 = FAUSTFLOAT(2e+01f * std::log10(std::max<float>(1.1754944e-38f, std::fabs(fTemp2))));
			float fTemp3 = fTemp2;
			output0[i0] = FAUSTFLOAT(fTemp3);
			fRec0[0] = std::max<float>(std::fabs(fTemp3), fConst1 * fRec0[1]);
			output1[i0] = FAUSTFLOAT(fRec0[0]);
			float fTemp4 = mydsp_faustpower2_f(fTemp3);
			fVec0[IOTA0 & 32767] = fTemp4;
			fRec1[0] = fTemp4 + fRec1[1] - fVec0[(IOTA0 - iConst4) & 32767];
			output2[i0] = FAUSTFLOAT(std::sqrt(std::max<float>(0.0f, fConst2 * fRec1[0])));
			int iTemp5 = iRec2[1] * (float(iRec2[1]) <= fConst3);
			int iTemp6 = fRec0[0] > 0.15848932f;
			iVec1[0] = iTemp6;
			iRec2[0] = (iTemp5 > 0) * (iTemp5 + 1) + (iTemp6 > iVec1[1]);
			int iTemp7 = (fConst2 * float(iRec2[0])) > 0.0f;
			iVec2[0] = iTemp7;
			output3[i0] = FAUSTFLOAT(iTemp7 > iVec2[1]);
			fRec0[1] = fRec0[0];
			IOTA0 = IOTA0 + 1;
			fRec1[1] = fRec1[0];
			iVec1[1] = iVec1[0];
			iRec2[1] = iRec2[0];
			iVec2[1] = iVec2[0];
		}
	}

};

#endif
