
# APPUNTI

Salve maestro sono incastrato in alcuni problemi con l'algoritmo. 
Ho scritto questo algoritmo che conta i picchi della campana. questi picchi poi verrano dati in pasto a un altro algoritmo che mi calcola la frequenza di percussione su un range temporale. 
Il segnale di controllo che ricavo poi lo rimappo in vari modi e mi serve principalmente per regolare l'apertura del Q del banco di filtri per la resintesi. 
Però la prima parte dell'algoritmo, ovvero il rilevatore dei picchi, mi sta dando dei problemi:

Gli appunti sono composti così. spiego come è fatto il rilevatore. e poi le spiego come leggere i file wav di analisi e cosa c'è dentro in modo da poter interpretare i segnali di controllo. Proseguiamo!

# Rilevatore picchi

Il rilevatore picchi è una funzione che ha come parametri in ingresso (un valore temporale, una soglia)ed è formato da:

 - 1. un **peakenvelope**: questa funzione ha due inputs (il tempo di integrazione e il segnale).Calcola il varlore massimo tra segnale e lo stesso ritardato di un campione moltiplicato per un coefficiente di attenuazione. Il coefficiente è ottenuto con la trasformata bilineare dal tempo di integrazione fornito in input. 

 - 2.  `(l'output del peakenvelope) > soglia`.

 - 3. **line**: una funzione che ha due inputs (la durata della line, il trigger ottenuto dalla precedente condizione booleana): questa funzione genera una rampa da 0 a 1 nel temp desiderato. 

 - 4. `(l'output di line) > 0` : mi genera un gradino unitario della durata della line.

 - 5. **diffTresh**: la funzione ha come input (l'output della precedente condizione booleana) e verifica se `input > input(ritardato di un campione)`. In questo modo mi genera un impulso ogni volta che si attiva un gradino unitario. 


Per una migliore comprensione c'è questo diagramma:


![Rilevatore Picchi - Diagramma a blocchi](2-WAV/2-WAV-SVG-IMMAGINI/rilevatore-0x600001e91b90.svg)



Queste funzioni tutte consecutive sono il rilevatore di picchi. Ogni volra che trova un picco, l'algoritmo è sordo per un deltaT (l'input temporale).
Il problema è che se il segnale in input ha un picco forte a tal punto che la risonanza della campana ha un valore > soglia dopo il tempo di sordità, scatta un altro picco.

\newpage

# File 1.wav

Nel file 1.wav ho fatto un test di analisi e sono presenti 6 segnali:

 a. la registrazione in tempo reale della campana,

 b. a che entra nel **peakenvelope** (1.)

 c. b che entra nella prima condizione booleana (2.)

 d. c che entra nella **line** (3.)

 e. d che entra nella condizione booleana (4.)

 f. e che entra nel **diffTresh** (5.)

Per una migliore comprensione c'è questo diagramma:

![1-WAV - Diagramma a blocchi dei segnali di controllo](1-WAV/1-WAV-SVG-IMMAGINI/1-WAV.svg)

\newpage

# File 2.wav


Nel file 2.wav sono presenti 4 segnali: 
 - la registrazione in tempo reale della campana, 

 - il risultato del **peakEnvelope** (con tempo di integrazione a 0.1),

 - un **movingAverageRMS** (con tempo di media a 0.1),

 - il **rilevatore di picchi** (con valore temporale di 0.1 e soglia 0.15)

Per una migliore comprensione c'è questo diagramma:

![2-WAV - Diagramma a blocchi dei segnali di controllo](2-WAV/2-WAV-SVG-IMMAGINI/2-WAV.svg)

\newpage

Come vede aumentando la potenza dell'impulso vengono generati dei picchi fantasma. Vorrei capire come sfruttare il valore RMS per creare una soglia mobile all'interno del rilevatore picco. 

Avevo pensato di fare il `minimo` tra l'RMS e 0.15. In questo modo l'RMS aumenta la soglia per percussioni catastrofiche e la diminuisce per adattarsi all'azione del percussionista. 
Non sto capendo proprio però come trattare il valore RMS per renderlo intelligente. 

Cosa posso fare??


\newpage


# Appendice

Nella cartella sono presenti i file .wav, tutte le immagini, e il dsp faust.
Se vuole dare un'occhiata al dsp faust le ricordo al volo alcune cose della sintassi:

 - `:` è la composizione in sequenza, ovvero colleghi l'output di ciò che sta a sinistra con l'input che sta a destra; 

 - `<:` fa una scomposizione parallela, splittando il segnale o i segnali;

 - `:>` è una composizione parallela, fa un merge di piu segnali in uno;

 - `@(x)` vuol dire ritardare di x campioni;

 - `~` è l'operatore di retroazione. ciò che sta a destra sta nel ramo di retroazione.

[se clicca qui c'è il link per una spiegazione piu dettagliata.](https://faustdoc.grame.fr/manual/syntax/index.html#diagram-composition-operations)

Inoltre qui lascio tutti i diagramma a blocchi delle varie funzioni in modo che siano comprensibili.

![PeakEnvelope](1-WAV/1-WAV-SVG-IMMAGINI/peakenvelope-0x600003f17840.svg)

![Line](1-WAV/1-WAV-SVG-IMMAGINI/line-0x600003f13e70.svg)


![feedback_dentro_line](1-WAV/1-WAV-SVG-IMMAGINI/feedb-0x600003f13d50.svg)


![feedforward_dentro_line](1-WAV/1-WAV-SVG-IMMAGINI/feedf-0x600003f12760.svg)


![difftresh](1-WAV/1-WAV-SVG-IMMAGINI/diffTresh-0x600003f1cab0.svg)


![difftresh](2-WAV/2-WAV-SVG-IMMAGINI/movingAverageRMS-0x600001ee87e0.svg)


![difftresh](2-WAV/2-WAV-SVG-IMMAGINI/movingAverage-0x600001ee81b0.svg)


![difftresh](2-WAV/2-WAV-SVG-IMMAGINI/pole-0x600001ee7e70.svg)
