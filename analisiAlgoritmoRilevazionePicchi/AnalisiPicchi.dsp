import("stdfaust.lib");

//======================================================================================================

input = par(i,2,(_ <: attach(_,abs : ba.linear2db : group(subgroup(vbargraph("Level %i",-60,0)))))) : + : (_ <: attach(_,abs : ba.linear2db : group(vbargraph("[2]Input Mid",-60,0))))
        with{
            subgroup(x) = hgroup("[0]input",x);
            group(x) = hgroup("sig",x);
        };
//======================================================================================================




//======================================================================================================
rilevatore(time,tresh) = (_ <: (peakenvelope(time) > tresh : (line(ma.SR*time) / (ma.SR*time)) > 0 : diffTresh));

peakenvelope(period, x) = loop ~ _
    with {
        loop(y) = max(abs(x), y * coeff);
        twoPIforT = (2.0 * ma.PI) * (1.0 / ma.SR);
        coeff = exp(-twoPIforT / max(ma.EPSILON, period));
    };


line(value, trigger) = (feedf ~ feedb)
 with 
    {   
        break = value;
        y = trigger > trigger';
        feedf(x) = (x > 0) + (x * (x > 0)) + y;
        feedb(x) = (x <= break) * x;
        offset(x) = ((x - 1) > 0) * (x - 1);
    };

diffTresh(x) = x > x';
//======================================================================================================





//======================================================================================================
// Moving Average Envelope Follower
movingAverage(seconds, x) = x - x@(seconds * ma.SR) : 
    fi.pole(1) / (seconds * ma.SR);
//process = _ * 10 : movingAverage(1);

// Moving Average RMS
movingAverageRMS(seconds, x) = sqrt(max(0, movingAverage(seconds, x ^ 2)));
//process = movingAverageRMS(1);
//======================================================================================================






//======================================================================================================
//  PARAMETRI PER MANIPOLARE IL GLI ALGORITMI
tresh = ba.db2linear(-16);
time = 0.1;
//======================================================================================================


//======================================================================================================
//
// PROCESSO 1.WAV       ---- scommentare per utilizzare ----
//
//process = input <: _, (peakenvelope(time) <: _ , (_> tresh <:_, (line(ma.SR*time) / (ma.SR*time)), ((line(ma.SR*time) / (ma.SR*time)) >0 ), ((line(ma.SR*time) / (ma.SR*time)) >0 :diffTresh)));
//======================================================================================================

//======================================================================================================
//
// PROCESSO 2.WAV       ---- scommentare per utilizzare ---- 
//
process = input <: _, peakenvelope(0.1), movingAverageRMS(0.1), rilevatore(0.1,tresh);
