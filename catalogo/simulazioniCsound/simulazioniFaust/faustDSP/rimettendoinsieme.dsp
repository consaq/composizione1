import("stdfaust.lib");
import("libreriaCampana.lib");
import("mspan.lib");
import("schroederAllpass.lib");

timeRMS = 0.1;
timePeak = .1;
pkTresh = -25 : ba.db2linear;
timeAvgPeakCounter=4;


//default values
// richiamare con dot notation
// def.fbTime
def = environment {
    timeRMS = 0.05;
    timePeak = .1;
    pkTresh = -25 : ba.db2linear;
    fbTime = .15;
    fbgI = .5;
};



// ---------
//  INPUT RISINTESI
inNoiseBank = par(i,6,inNoise) ;
inPulseBank = si.bus(6) : par(i,6,inPulse) ;
inNoise = (no.noise*0.5);
inPulse(in) = ba.pulsen(10, ma.SR/(3+(freq2(in))+5*(1-in))) ;
inPulse2(in) = ba.pulsen(10, ma.SR/(6+(freq3(in)))) ;
// frequenza del pulsen
//.  --> con sequence
freq(in) = frequencyPeak(0.1,0.15,2,2,in)*2 , 17: min;
//.  --> con chord
freq2(x) = peakenvelope(.2,x)*15 , 15: min ;
freq3(in) = in:frequenzaPicchi(timePeak,pkTresh,.6,5)^2 : peakHolder(4);

// ---------

//process = + : freq3;


// ---------
// in baase a come voglio che cambi il sequencer
// 
// chooseFilter mi permette di cambiare casualmente ogni volta che percuoto
chooseFilter = rilevatorePicchi(0.1,0.15) : sah(no.noise) :+(1):/(2):* (6) : min(6) : int;
// chooser ascolta la frequenza con cui percuoto e cambia alla stessa frequenza.
chooser(seed,in) = sahNoise((frequenzaPicchi(0.1,timePeak,1.5,2,in),6:min),seed)+1 : /(2): *(6):int;
chooserFbk(fbTime,fbgI,seed,in) = sahNoise((frequenzaPicchi(0.1,timePeak,1.5,2,in),6:min),seed)+1 : /(2): *(6) : delayFbk(fbTime,fbgI);
sampHold(in) = no.noise+1:/(2):*(6):ba.sAndH(rilevatorePicchi(0.1,0.15,in));
// ---------








// ---------
//-- NOISE
noiseBank(qFilBank) = bpBankPeakEnv , (inNoiseBank : bpBank(qFilBank)) : routingEnv;
noiseFbkBank(freqDev,fbTime,fbgI,qFilBank,in) = (in : bpBankPeakEnv : bpDelEnv(fbTime,fbgI)), (inNoiseBank : bpBankFmul(fDev,qFilBank)) : routingEnv
    with{
        condition = freqDev == 1;
        else = freqDev+(freqDev/2)*(movingAverageRMS(.05,in):peakHolder(6));
        fDev = ba.if(condition, 1, else);
    };
// ---------
noiseFbkBank2(fDev,freqDev,fbTime,fbgI,qFilBank,in) = (in : bpBankPeakEnv : bpDelEnv(fbTime,fbgI)), (inNoiseBank : bpBankFmul(fDev(freqDev,in),qFilBank)) : routingEnv;



// ---------
//-- IMPULSE
impulseBank(qFilBank) = bpBankPeakEnv <:  (inPulseBank : bpBank(qFilBank)), si.bus(6) : routingEnv;
impulseFbkBank(freqDev,fbTime,fbgI,qFilBank,in) = in : bpBankPeakEnv : bpDelEnv(fbTime,fbgI) <:  
                            (inPulseBank : bpBankFmul(fDev,qFilBank)), 
                            si.bus(6) : routingEnv
                            with{
                                //fDev = freqDev+(freqDev/2)*(movingAverageRMS(.05,in):peakHolder(6));
                                fDev = freqDev;
                                //fDev = freqDev-(freqDev/10)*(movingAverageRMS(.1,in):peakHolder(6));
                            };
// ---------
impulseFbkBank2(fDev,freqDev,fbTime,fbgI,qFilBank,in) = in : bpBankPeakEnv : bpDelEnv(fbTime,fbgI) <:  
                            (inPulseBank : bpBankFmul(fDev(freqDev,in),qFilBank)), 
                            si.bus(6) : routingEnv;



// ---------
// funzioni componenti delle funzioni *Bank e *FbkBank
bpDelEnv(fbTime,fbgI) = par(i,6,delayFbk(fbTime,fbgI/*(+i*0.05)*/));
routingEnv(i1,i2,i3,i4,i5,i6,e1,e2,e3,e4,e5,e6) = i1*e1,i2*e2,i3*e3,i4*e4,i5*e5,i6*e6;
makeMidSide2(i,degI) = mspan(.7,(degI*ma.PI/180));
makeMidSide(i,degI) = mspan(1,(degI*ma.PI/180)*i*((os.osc(.1+i*.01)*.1+.9)));

makeMidSide4(i,degI,in) = in : mspan(.6,((ma.signum(degI)*((abs(degI)+radianFun(.5,2,in)+radianFun2(in)^2)*2)*ma.PI/180)));

makeMidSide3(i,degI) = mspan(.9,(degI*ma.PI/180)+((degI*ma.PI/180)/(i+1)));

// ---------

radianFun2(in) = in <: (movingAverageRMS(.1)*50 : integrator(1.2));
radianFun(secCounter,sec,in) = in : frequenzaPicchi(timePeak,pkTresh,secCounter,1)^2.3 ;

//process = _ <: _,_,((abs(1)+radianFun(.3,2)+radianFun2^2));
//process = _ <: _, _,radianFun2;


// ---------
// FUNZIONI PRINCIPALI
chord = \(fx,fx2,degI).(_ <: (fx,_ : fx2 : par(i,6,makeMidSide(i,degI)):> si.bus(2): sdmx));
sequencer = \(fx,fx2,fx3,degI,in).( in <: (fx,_ : fx2 : ba.selectxbus(1,6, 8192,fx3(in))):makeMidSide2(fx3(in),degI):sdmx);
// ---------
chord2 = \(fx,fx2,degI,mside,tSec,gainRev,in).(in <: (fx,in : fx2 : par(i,6,mside(i,degI)):> si.bus(2) : _, combApf(tSec,gainRev): sdmx));
chord1 = \(fx,fx2,degI,mside,in).(in <: (fx,in : fx2 : par(i,6,mside(i,degI)):> si.bus(2) : sdmx));
//dflApf(t,g)
//dflApf(t,g) = ((+ : @(t-1) :apfSeries(t,g,5)) ~ *(g)) : mem;
g = 1/sqrt(2); // il massimo della quadratura tra i coefficienti allpass si ottiene con 1/sqrt(2)
t= ba.sec2samp(.07);



//freqList = (713,1844,3284,4946,6788,7744);
fDev(freqDev,in) = freqDev+(freqDev/2)*(movingAverageRMS(.05,in):peakHolder(6));
fDev2(freqDev,in) = freqDev;
fDev3(freqDev,in) = freqDev+(freqDev/15)*(peakenvelope(.2,in):peakHolder(6));





//process=0;







//============================================================================================================
//                 TERZO MONDO
//
// process multiCanale per Export
/*
process = + <:  si.bus(2),
                (chord(mappingQfilter2(2),noiseFbkBank(1,ba.samp2sec(3456),.5),32),
                    chord(mappingQfilter3(1.5),noiseFbkBank(1.3,ba.samp2sec(3456),.5),32) : lrsomma),
                (chord(mappingQfilter3(0.7),noiseFbkBank(.7,0.15,.4),-23),
                    chord(mappingQfilter3(1.5),noiseFbkBank(.5,0.05,.5),-45) : lrsomma);
*/
// process multiCanale per ascolto sulla Ide
/*
process = + <:  (si.bus(2):par(i,2,*(.3))),
                (chord2(mappingQfilter5(1.5),noiseFbkBank2(fDev3,.8,0.1,0),1,makeMidSide4,2,.3),
                    chord2(mappingQfilter5(1.5),noiseFbkBank2(fDev2,.8,.06,0),-1,makeMidSide4,2,.3) : lrsomma),
                (chord2(mappingQfilter6(1.7),noiseFbkBank2(fDev,1.4,0.1,0),-12,makeMidSide4,3,.4),
                    chord2(mappingQfilter6(1.5),noiseFbkBank2(fDev2,1.4,0.05,0),14,makeMidSide4,3,.4) : lrsomma)
                : par(i,2,*(checkbox("[0]input")):*(volumePlus)), par(i,2,*(checkbox("1")):*(volumePlus)), par(i,2,*(checkbox("2")):*(volumePlus)):>  si.bus(2) ;
volumePlus = nentry("vol",1,1,9,0.1):si.smoo;
*/

//============================================================================================================















//============================================================================================================
//                 PRIMO MONDO
// process multiCanale per Export
/*
process = + <:  (si.bus(2)),
                ((chord1(mappingQfilter4(1.5),impulseFbkBank(.6,0.05,.0),1,makeMidSide2)),
                    (chord1(mappingQfilter4(1.5),impulseFbkBank(.8,0.05,0),3,makeMidSide2)):lrsomma),                
                ((chord1(mappingQfilter5(1.5),impulseFbkBank(1.4,0.1,.2),5,makeMidSide2)),
                    chord1(mappingQfilter5(1.5),impulseFbkBank(1.2,0.1,.2),-12,makeMidSide2):lrsomma);
*/
// process multiCanale per ascolto sulla Ide
/*
process = + <:  (si.bus(2):par(i,2,*(0.05))),
                ((chord1(mappingQfilter4(1.5),impulseFbkBank(.6,0.05,.0),ba.take(1,deg),makeMidSide2)),
                    (chord1(mappingQfilter4(1.5),impulseFbkBank(.8,0.05,0),ba.take(2,deg),makeMidSide2)):lrsomma),                
                ((chord1(mappingQfilter5(1.5),impulseFbkBank(1.4,0.1,.2),ba.take(3,deg),makeMidSide2)),
                    chord1(mappingQfilter5(1.5),impulseFbkBank(1.2,0.1,.2),ba.take(4,deg),makeMidSide2):lrsomma)
                : par(i,2,*(checkbox("[0]input")):*(volumePlus)), par(i,2,*(checkbox("1")):*(volumePlus)), par(i,2,*(checkbox("2")):*(volumePlus)):>  si.bus(2) ;
volumePlus = nentry("vol",1,1,9,0.1):si.smoo;
deg = (par(i,4,0-hslider("deg %i",0,-180,180,1))); 
*/
//============================================================================================================



















//============================================================================================================
//                 SECONDO MONDO
// process multiCanale per Export
/*
process = + <:  (si.bus(2):par(i,2,*(0.2))),
                (chord2(mappingQfilter5(1.5),impulseFbkBank2(fDev2,.6,0.1,0.1),2,makeMidSide4,6,.5),
                    chord2(mappingQfilter5(1.5),impulseFbkBank2(fDev2,.8,.06,.2),-1,makeMidSide4,6,.5):lrsomma ),
                ((chord2(mappingQfilter5(1.5),impulseFbkBank2(fDev,1.4,0.12,.1),1,makeMidSide4,3,.4)),
                    chord2(mappingQfilter5(1.5),impulseFbkBank2(fDev,1.2,0.14,.1),-1,makeMidSide4,3,.4):lrsomma);
*/
// process multiCanale per ascolto sulla Ide
/*
process = + <:  (si.bus(2):par(i,2,*(0.2))),
                (chord2(mappingQfilter5(1.5),impulseFbkBank2(fDev2,.6,0.1,0.1),2,makeMidSide4,6,.5),
                    chord2(mappingQfilter5(1.5),impulseFbkBank2(fDev2,.8,.06,.2),-1,makeMidSide4,6,.5):lrsomma ),
                ((chord2(mappingQfilter5(1.5),impulseFbkBank2(fDev,1.4,0.12,.15),1,makeMidSide4,3,.6)),
                    chord2(mappingQfilter5(1.5),impulseFbkBank2(fDev,1.2,0.14,.15),-1,makeMidSide4,3,.6):lrsomma):
                     par(i,2,*(checkbox("[0]input")):*(volumePlus)), par(i,2,*(checkbox("1")):*(volumePlus)),par(i,2,*(checkbox("2")):*(volumePlus)):>  si.bus(2);
volumePlus = nentry("vol",1,1,9,0.1):si.smoo;
*/
//bal = hslider("m-s", 0.5, 0, 1, 0.01):*(90):si.smoo;
//balance(m,s) = m * cos(bal), s * sin(bal);
// : sdmx : balance : par(i,2,*(2)) : sdmx

//============================================================================================================



















/*
process = + <:  si.bus(2),(chord(mappingQfilter3(2),impulseFbkBank(.3,ba.samp2sec(3456),.6),10),
                chord(mappingQfilter3(1.5),impulseFbkBank(.85,ba.samp2sec(1024),.6),24):lrsomma),
                (chord(mappingQfilter3(2),impulseFbkBank(.7,0.1,.4),10),
                chord(mappingQfilter3(1.5),impulseFbkBank(1.6,0.1,.4),24):lrsomma);
*/










/*process = + <:  si.bus(2),(sequencer(mappingQfilter2(2),impulseFbkBank(.3,ba.samp2sec(3456),.4),chooser(4732),32),
                sequencer(mappingQfilter2(1.5),impulseFbkBank(.85,ba.samp2sec(1024),.2),chooser(1823),12):lrsomma),
                (sequencer(mappingQfilter3(2),impulseFbkBank(.7,0.05,.4),chooser(9303),-23),
                sequencer(mappingQfilter3(1.5),impulseFbkBank(1.6,0.05,.4),chooser(2843),-45):lrsomma);*/

//process = + <: si.bus(2), sequencer(mappingQfilter2(2),noiseBank,chooseFilter,24);

/*process = + <:  si.bus(2),(sequencer(mappingQfilter2(2),noiseFbkBank(1,ba.samp2sec(3456),.5),chooseFilter,32),
                (sequencer(mappingQfilter3(1.5),noiseFbkBank(1.3,ba.samp2sec(3456),.5),chooseFilter,32)):lrsomma),
                (sequencer(mappingQfilter3(0.7),noiseFbkBank(.7,0.15,.4),chooseFilter,-23),
                sequencer(mappingQfilter3(1.5),noiseFbkBank(.5,0.05,.5),chooseFilter,-45):lrsomma);*/


/*process = + <:  si.bus(2),chord(mappingQfilter4(1.5),noiseFbkBank(.85,ba.samp2sec(1024),.6),-52),
                sequencer(mappingQfilter2(1.5),noiseFbkBank(1.1,ba.samp2sec(1024),.72),chooseFilter,24),
                (chord(mappingQfilter2(1.5),impulseFbkBank(.6,.08,.6),10)<: si.bus(2), par(i,2,de.delay(ma.SR*12,ma.SR*12))),
                (sequencer(mappingQfilter5(1.5),impulseFbkBank(.9,0.1,.4),chooser(324),24)<: si.bus(2), par(i,2,de.delay(ma.SR*7,ma.SR*7)));*/


// solitamente al process di partenza al posto del più metto per avere idea di cosa succede e calibrare la treshold. solitamente la imposto a .15
//process = input : inputMid;


//process = + <: mappingQfilter3(1.5), mappingQfilter2(2), mappingQfilter4(1.5),mappingQfilter5(1.5);


// ---------
// FUNZIONI DA INSERIRE NELLA LIBRERIA 
// segnali di controllo del Q dei filtri bandpass
mappingQfilter6(secCounter) = ma.E^frequenzaPicchi(timePeak,pkTresh,secCounter,2)*2: _+400 : _ ,1000 : min : peakHolder(3) : integrator(0.3);
mappingQfilter5(secCounter) = ma.E^frequenzaPicchi(timePeak,pkTresh,secCounter,2)*2: _+200 : _ ,1000 : min : peakHolder(3) : integrator(0.3);
mappingQfilter4(secCounter) = ma.E^frequenzaPicchi(timePeak,pkTresh,secCounter,2)*2: _+50 : _ ,1000 : min : peakHolder(7) : integrator(0.3);
mappingQfilter3(secCounter) = ma.E^frequenzaPicchi(timePeak,pkTresh,secCounter,2): _+50 : _ ,1000 : min : peakHolder(3) : integrator(0.3);
mappingQfilter2(secCounter) = ma.E^frequenzaPicchi(timePeak,pkTresh,secCounter,2) : _+50 : _ ,1000 : min : integrator(0.3);
//TEST
//process = + <: _,mappingQfilter2(3), mappingQfilter3(1.5), mappingQfilter4(1.5),mappingQfilter5(1.5);
// ---------




// ---------
// FUNZIONI CHIAMATI NEL PROCESS PER IL ROUTING
lrsomma = \(l1,r1,l2,r2).(l1+l2,r1+r2);
// ---------
xAscoltare(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14) = (a3+a5+a7+a9+a11+a13)/4,(a4+a6+a8+a10+a12+a14)/4;
spegnimi = par(i,2,*(0));
// ---------





//_____________________ Luca Spanedda LIBRARY __________________________
//
// binary selector
selector(sel, x, y) = x * (1 - sel) + y * (sel);
// a classic sample and hold
sah(x, t) = selector(t, _, x) ~ _;
// random number generator
random(range, seed, t) = ((noise(seed) : abs), t) : sah * range;
//dirac
dirac = 1-1';
// first derivate
derivate(x) = x < x';
// pseudo-random noise with linear congruential generator (LCG)
noise(initSeed) = lcg ~ _ : (_ / m)
with{
    a = 18446744073709551557; c = 12345; m = 2 ^ 31; 
    lcg(seed) = ((a * seed + c) + (initSeed - initSeed') % m);
};
// sample and hold with internal trigger
sahSinc(f, x) = (x, pulseTrain(f)) : sah;
pulseTrain(f) = phasor0(f) : derivate + dirac;
// sah noise
sahNoise(f, seed) = sahSinc(f, noise(seed));
// phasor that start from 0
phasor0(f) = (_ <: _ + f, _) ~  _ % ma.SR : (!, _ / ma.SR);
//
//__________ LS LIBRARY __________________________________________________